//
//  Dices.h
//  Dice
//
//  Created by Alain Pitre on 2016-05-29.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#ifndef Dices_h
#define Dices_h

#import "Dice.h"

@interface Dices : NSObject

- (void) create: (UIButton*) instance;

- (NSMutableArray*) getList;

- (int) getTotal;

- (void) updateTotal: (Dice*) dice;

- (Dice*) getDiceAtIndex: (int) index;

- (void) roll;

- (void) fakeRoll: (int)d1 d2:(int)d2 d3:(int)d3;

- (BOOL) hasValues: (NSArray*) values;

- (BOOL) hasSamesValues;

- (BOOL) isPrimeNumber;

@end

#endif /* Dices_h */
