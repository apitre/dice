//
//  Dice.m
//  Dice
//
//  Created by Alain Pitre on 2016-05-13.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Dice.h"

@implementation Dice {
    int value;
    int nbFaces;
    int* faces;
    UIButton* button;
    BOOL isActive;
    NSArray *facesAnimation[6];
}

- (id) init {
    self = [super init];
    
    if (self) {
        nbFaces = 6;
        value = 0;
        [self initFaces];
        [self generateRandomValue];
        [self activate];
        [self initAnimatedFrame];
    }
    
    return self;
}

- (void) setNumber: (int)num {
    value = num;
}

- (void) setUiButton: (UIButton*) uibutton {
    button = uibutton;
    [self animate];
}

- (void) initAnimatedFrame {
    NSMutableArray *frames = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 13; i++) {
        UIImage *frame = [UIImage imageNamed:[NSString stringWithFormat:@"frame-%d.png", i + 1]];
        [frames addObject:frame];
    }
    
    for (int i = 0; i < 6; i++) {
        NSMutableArray *images = [[NSMutableArray alloc] init];
        
        [images addObjectsFromArray: frames];
        [images addObject: [UIImage imageNamed:[NSString stringWithFormat:@"face-%d.png", i + 1]]];
        
        facesAnimation[i] = images;
    }
}

- (void) roll: (float) delay {
    if (isActive) {
        [self generateRandomValue];
        [self performSelector:@selector(animate) withObject:nil afterDelay:delay];
    } else {
        [self onFinishAnimate];
    }
}

- (void) animate {
    NSArray *images = facesAnimation[value - 1];
    
    button.imageView.animationImages = images;
    button.imageView.animationRepeatCount = 1;
    button.imageView.animationDuration = 2;
    
    [button setImage:images.lastObject forState:UIControlStateNormal];
    
    [self
        performSelector: @selector(onFinishAnimate)
        withObject: nil
        afterDelay: button.imageView.animationDuration
    ];
    
    [button.imageView startAnimating];
}

- (void) onFinishAnimate {
    [[NSNotificationCenter defaultCenter]
        postNotificationName: @"onFinishAnimate"
        object: self
    ];
}

- (BOOL) isActive {
    return isActive;
}

- (void) generateRandomValue {
    value = faces[(arc4random() % nbFaces)];
}

- (void) toggleActivity {
    if ([self isActive]) {
        [self desactivate];
    } else {
        [self activate];
    }
}

- (int) getNumber {
    return value;
}

- (void) activate {
    isActive = true;
    button.alpha = 1;
}

- (void) desactivate {
    isActive = false;
    button.alpha = 0.6;
}

- (void) setTag: (int)tag {
    [button setTag:tag];
}

- (int) getTag {
    return [button tag];
}


- (void) initFaces  {
    faces = malloc(sizeof(int) * nbFaces);
    for (int i = 0; i < nbFaces; i++) {
        faces[i] = i + 1;
    }
}

@end
