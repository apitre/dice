//
//  ContractGoldNumber.m
//  Dice
//
//  Created by Alain Pitre on 2016-05-28.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContractGoldNumber.h"

@implementation ContractGoldNumber {
    NSArray* gold;
}

- (id) init {
    self = [super init];
    
    if(self) {
        [super setName:@"Or"];
        gold = @[@1, @5, @2];
    }
    
    return self;
}

- (void) validate: (Dices*) dices {
    
    int total = 0;
    
    if ([dices hasValues:gold]) {
        total = ([[dices getDiceAtIndex:0] getNumber] * 10);
    }
    
    [self setValue:total];
    
}

@end