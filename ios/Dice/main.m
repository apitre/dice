//
//  main.m
//  Dice
//
//  Created by Alain Pitre on 2016-05-13.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
