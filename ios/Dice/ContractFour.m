//
//  ContractFour.m
//  Dice
//
//  Created by Alain Pitre on 2016-05-28.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContractFour.h"

@implementation ContractFour {
    
}

- (id) init {
    self = [super init];
    
    if(self) {
        [super setName:@"4"];
    }
    
    return self;
}

- (void) validate: (Dices*) dices {
    
    int total = 0;
    
    if ([self isDivisible:total by:4]) {
        total = [dices getTotal];
    }
    
    [self setValue:total];
    
}

@end