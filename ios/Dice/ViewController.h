//
//  ViewController.h
//  Dice
//
//  Created by Alain Pitre on 2016-05-13.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *diceBtn1;

@property (weak, nonatomic) IBOutlet UIButton *diceBtn2;

@property (weak, nonatomic) IBOutlet UIButton *diceBtn3;

@property (weak, nonatomic) IBOutlet UILabel *labelTotal;

@property (weak, nonatomic) IBOutlet UIButton *retry;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)tap:(id)sender;

- (IBAction)retry:(id)sender;

@end

