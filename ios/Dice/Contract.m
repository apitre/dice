//
//  Contract.m
//  Dice
//
//  Created by Alain Pitre on 2016-05-24.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Contract.h"

@implementation Contract {
    int value;
    NSString* title;
}

- (void) validate: (Dices*) dices {
    NSLog(@"Validation not implemented yet!");
}

- (int) getValue {
    return value;
}

- (NSString*) getName {
    return title;
}

- (void) setName: (NSString*) name {
    title = name;
}

- (void) setValue: (int) numeric {
    value = numeric;
}

- (BOOL) isDivisible: (int)numerator by:(int)denominator {    
    return (numerator % denominator) == 0;
}

@end