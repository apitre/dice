//
//  Dice.h
//  Dice
//
//  Created by Alain Pitre on 2016-05-13.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#ifndef Dice_h
#define Dice_h

#import <UIKit/UIKit.h>

@interface Dice : NSObject

- (void) generateRandomValue;

- (int) getNumber;

- (void) setNumber: (int)num;

- (BOOL) isActive;

- (void) activate;

- (void) desactivate;

- (void) toggleActivity;

- (void) roll: (float) delay;

- (void) setTag: (int)tag;

- (int) getTag;

- (void) setUiButton: (UIButton*) uibutton;

@end

#endif /* Dice_h */
