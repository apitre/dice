//
//  ContractChance.m
//  Dice
//
//  Created by Alain Pitre on 2016-05-28.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContractChance.h"

@implementation ContractChance {
    
}

- (id) init {
    self = [super init];
    
    if(self) {
        [super setName:@"Chance"];
    }
    
    return self;
}

- (void) validate: (Dices*) dices {
    
    int total = [dices getTotal];
    
    [self setValue:total];
    
}

@end