//
//  ContractPrimeNumber.m
//  Dice
//
//  Created by Alain Pitre on 2016-05-28.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContractPrimeNumber.h"

@implementation ContractPrimeNumber {
    
}

- (id) init {
    self = [super init];
    
    if(self) {
        [super setName:@"Premier"];
    }
    
    return self;
}

- (void) validate: (Dices*) dices {
    
    int total = [dices getTotal];
    
    NSLog(@"is prime number: %d", [dices isPrimeNumber]);
    
    if ([dices isPrimeNumber] == false) {
        total = 0;
    }
    
    [self setValue:total];
    
}

@end