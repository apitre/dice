//
//  MultiColumnTableViewCell.h
//  Dice
//
//  Created by Alain Pitre on 2016-05-29.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#ifndef MultiColumnTableViewCell_h
#define MultiColumnTableViewCell_h

#import <UIKit/UIKit.h>

@interface MultiColumnTableViewCell : UITableViewCell
@property (strong, nonatomic) UILabel *label1;
@property (strong, nonatomic) UILabel *label2;
@property (strong, nonatomic) UILabel *label3;
@end

#endif /* MultiColumnTableViewCell_h */
