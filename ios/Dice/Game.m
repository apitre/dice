//
//  Board.m
//  Dice
//
//  Created by Alain Pitre on 2016-05-17.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Game.h"
#import "Player.h"
#import "Dice.h"

@implementation Game {
    Player* players[2];
    Player* activePlayer;
    Dices* dices;
    int hits;
    UILabel *total;
    UITableView *tableView;
    NSMutableArray *contracts;
}

- (id) init {
    self = [super init];
    
    if(self) {
        hits = 0;
        activePlayer = players[0];
    }
    
    return self;
}

- (void) setDices: (Dices*) instance {
    dices = instance;
}

- (int) getTotalContracts {
    return [contracts count];
}

- (void) setTableView: (UITableView*) table {
    tableView = table;
}

- (void) setContracts: (NSMutableArray*) contractsArray {
    contracts = contractsArray;
}

- (Contract*) contractAtIndex: (int) index {
    return [contracts objectAtIndex:index];
}

- (void) setLabelTotal: (UILabel*) label {
    total = label;
}

- (void) rollDices {
    [dices roll];
}

- (Dice*) getDice: (int)tag {
    return [dices getDiceAtIndex:tag];
}

- (void) handleOnFinishAnimate:(NSNotification *) notification {
    Dice* dice = notification.object;
    
    [self updateTotal: dice];
    
    if ([dice getTag] == 2) {
        [self updateContracts];
        [tableView reloadData];
    }
}

- (void) updateTotal:(Dice*) dice  {
    [dices updateTotal:dice];
    total.text = [NSString stringWithFormat:@"%d", [dices getTotal]];
}

- (void) updateContracts {
    for (int i = 0; i < [self getTotalContracts]; i++) {
        [[contracts objectAtIndex:i] validate:dices];
    }
}

- (void) setNextPlayer {
    int nextPlayer = ([activePlayer getId] + 1);
    
    if (nextPlayer == ((sizeof players) / (sizeof players[0]))) {
        nextPlayer = 0;
    }
    
    activePlayer = players[nextPlayer];    
}

- (void) createPlayer: (int)p_id instance:(Player*)player {
    players[p_id] = player;
    [player setId:p_id];
}

@end