//
//  ContractPrimeNumber.h
//  Dice
//
//  Created by Alain Pitre on 2016-05-28.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#ifndef ContractPrimeNumber_h
#define ContractPrimeNumber_h

#import "Contract.h"

@interface ContractPrimeNumber : Contract


@end

#endif /* ContractPrimeNumber_h */
