//
//  Board.h
//  Dice
//
//  Created by Alain Pitre on 2016-05-17.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#ifndef Player_h
#define Player_h

#import <UIKit/UIKit.h>

@interface Player : NSObject

- (void) setId: (int)number;

- (int) getId;

@end

#endif /* Board_h */
