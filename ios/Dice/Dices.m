//
//  Dices.m
//  Dice
//
//  Created by Alain Pitre on 2016-05-29.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Dices.h"
#import "Dice.h"

@implementation Dices {
    NSMutableArray *dices;
    int total;
    float delay;
}

- (id) init {
    self = [super init];
    
    if(self) {
        dices = [[NSMutableArray alloc] init];
        delay = 0.2;
    }
    
    return self;
}

- (void) updateTotal: (Dice*) dice {
    total += [dice getNumber];
}

- (void) roll {
    total = 0;
    int decal = 0;
        
    for (id dice in dices) {
        if ([dice isActive]) {
            decal++;
        }
        [dice roll:(delay * decal)];
    }
}

- (Dice*) getDiceAtIndex: (int) index {
    return [dices objectAtIndex:index];
}

- (NSMutableArray*) getList {
    return dices;
}

- (int) getTotal {
    return total;
}

- (BOOL) hasNumber: (int)number in:(NSArray*) array {
    for (id nsNumber in array) {
        if ([nsNumber integerValue] == number) {
            return true;
        }
    }
    return false;
}

- (BOOL) hasValues: (NSArray*) values {
    NSMutableArray* finded = [[NSMutableArray alloc] init];
    int number;
    
    for (id dice in dices) {
        number = [dice getNumber];
        
        if ([self hasNumber:number in:values] && ![self hasNumber:number in:finded]) {
            [finded addObject: @(number)];
        }
        
    }
    
    return ([finded count] == [values count]);
}

- (BOOL) hasSamesValues {
    int firstValue = [[self getDiceAtIndex:0] getNumber];
    
    for (id dice in dices) {
        if ([dice getNumber] != firstValue) {
            return false;
        }
    }
    
    return true;
}

- (BOOL) isPrimeNumber {    
    for (int i = 2; i < total -1; i++) {
        if (total % i == 0) {
            return false;
        }
    }
    return true;
}

- (void) fakeRoll: (int)d1 d2:(int)d2 d3:(int)d3 {
    [[self getDiceAtIndex:0] setNumber:d1];
    [[self getDiceAtIndex:1] setNumber:d2];
    [[self getDiceAtIndex:2] setNumber:d3];
}

- (void) create: (UIButton*) instance; {
    Dice* dice = [[Dice alloc] init];
    [dice setUiButton:instance];
    [instance setTag:[dices count]];
    [dices addObject:dice];
}

@end
