//
//  Board.h
//  Dice
//
//  Created by Alain Pitre on 2016-05-17.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#ifndef Game_h
#define Game_h

#import "Player.h"
#import "Dices.h"
#import "Contract.h"

#import <UIKit/UIKit.h>

@interface Game : NSObject

- (void) setNextPlayer;

- (int) getTotalContracts;

- (void) setTableView: (UITableView*) table;

- (void) setContracts: (NSMutableArray*) contractsArray;

- (Contract*) contractAtIndex: (int) index;

- (void) setLabelTotal: (UILabel*) label;

- (Dice*) getDice: (int)tag;

- (void) handleOnFinishAnimate: (Dice*) dice;

- (void) rollDices;

- (void) setDices: (Dices*) instance;

- (void) createPlayer: (int)id instance:(Player*)newplayer;

@end

#endif /* Board_h */
