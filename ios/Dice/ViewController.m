//
//  ViewController.m
//  Dice
//
//  Created by Alain Pitre on 2016-05-13.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import "ViewController.h"
#import "Dice.h"
#import "Game.h"
#import "Dices.h"
#import "Player.h"
#import "MultiColumnTableViewCell.h"
#import "ContractOne.h"
#import "ContractTwo.h"
#import "ContractTree.h"
#import "ContractFour.h"
#import "ContractFive.h"
#import "ContractSix.h"
#import "ContractPi.h"
#import "ContractPrimeNumber.h"
#import "ContractGoldNumber.h"
#import "ContractBrelan.h"
#import "ContractChance.h"
#import "Contract.h"

@interface ViewController ()

@end

@implementation ViewController {
    Game* game;
}

- (IBAction)flipDices:(id)sender {
    //if (hits < 3) {
        //hits++;
        [self runFlip];
    //}    
}

- (void)runFlip {
    [game rollDices];
    [game setNextPlayer];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self builGame];
}

- (void) builGame {
    
    game = [[Game alloc] init];
    
    [game createPlayer:0 instance:[[Player alloc] init]];
    
    [game createPlayer:1 instance:[[Player alloc] init]];
    
    [game setLabelTotal: _labelTotal];
    
    [game setTableView:_tableView];
    
    [game setContracts: [self buildContracts]];
    
    [game setDices:[self buildDices]];
    
    [[NSNotificationCenter defaultCenter]
        addObserver: game
           selector: @selector(handleOnFinishAnimate:)
               name: @"onFinishAnimate"
             object: nil
     ];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [game getTotalContracts];
}

- (UITableViewCell *) tableView: (UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MultiColumnTableViewCell *cell = [[MultiColumnTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell" ];
    
    Contract* contract = [game contractAtIndex:(int)indexPath.row];
    
    cell.label1.text = [contract getName];
    cell.label2.text = [NSString stringWithFormat:@"%d", [contract getValue]];
    cell.label3.text = [NSString stringWithFormat:@"%d", [contract getValue]];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

- (NSMutableArray*) buildContracts {
    NSMutableArray *contracts = [[NSMutableArray alloc] init];
    
    [contracts addObject:[[ContractOne alloc] init]];
    [contracts addObject:[[ContractTwo alloc] init]];
    [contracts addObject:[[ContractTree alloc] init]];
    [contracts addObject:[[ContractFour alloc] init]];
    [contracts addObject:[[ContractFive alloc] init]];
    [contracts addObject:[[ContractSix alloc] init]];
    [contracts addObject:[[ContractPi alloc] init]];
    [contracts addObject:[[ContractBrelan alloc] init]];
    [contracts addObject:[[ContractGoldNumber alloc] init]];
    [contracts addObject:[[ContractPrimeNumber alloc] init]];
    [contracts addObject:[[ContractChance alloc] init]];
    
    return contracts;
}

- (Dices*) buildDices {
    Dices *dices = [[Dices alloc] init];
    [dices create: _diceBtn1];
    [dices create: _diceBtn2];
    [dices create: _diceBtn3];
    return dices;
}

- (IBAction)tap:(id)sender {
    Dice* dice = [game getDice:(int)[sender tag]];
    [dice toggleActivity];
}

- (IBAction)retry:(id)sender {
    [self runFlip];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
