//
//  ContractTwo.m
//  Dice
//
//  Created by Alain Pitre on 2016-05-28.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContractPi.h"
#import "Dice.h"

@implementation ContractPi {
    NSArray* PI;
}

- (id) init {
    self = [super init];
    
    if(self) {
        [super setName:@"PI"];
        PI = @[@3, @1, @4];
    }
    
    return self;
}

- (void) validate: (Dices*) dices {
    
    int total = 0;
    
    if ([dices hasValues:PI]) {
        total = ([[dices getDiceAtIndex:0] getNumber] * 10);
    }
    
    [self setValue:total];
    
}

@end