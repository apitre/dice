//
//  Contract.h
//  Dice
//
//  Created by Alain Pitre on 2016-05-24.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#ifndef Contract_h
#define Contract_h

#import "Dices.h"

@interface Contract : NSObject {
    
}

- (void) setName: (NSString*) name;

- (void) setValue: (int) numeric;

- (int) getValue;

- (NSString*) getName;

- (BOOL) isDivisible: (int)numerator by:(int)denominator;

- (void) validate: (Dices*) dices;

@end

#endif /* Contract_h */
