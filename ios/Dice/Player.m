//
//  Board.m
//  Dice
//
//  Created by Alain Pitre on 2016-05-17.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Player.h"
#import "Contract.h"

@implementation Player {
    int p_id;
    NSMutableArray *contracts;
}

- (id) init {
    self = [super init];
    
    if(self) {
        contracts = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void) setId: (int)number {
    p_id = number;
}

- (int) getId {
    return p_id;
}

@end